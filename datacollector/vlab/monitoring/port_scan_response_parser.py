import re

from tcpclientserver.server import SEPARATOR, RESPONSE_TERMINATION_CHARS

#from . import *

def extract_host_substring(logger, parts):
    return parts[0]

def extract_ports_array(logger, parts):
    if len(parts) == 1:
        # A Server with No Open Ports
        #logger.debug("A Server with ZERO Open-Ports ... %s", parts[0])
        return ""

    p_i = parts[1]

    logger.debug(f"Managing Port Scan Results with Dr. Portscan --> {p_i}")
    mesg = re.search("Ports:(.+?)Ignored State", p_i)

    m_sg = re.search("Ports:(.+?)@endofresponse@@", p_i)
    if not mesg and m_sg:
        mesg = m_sg
        
    logger.debug(f"Open Ports Parser ... this is the important-all string --> {mesg}")
    logger.debug(f"Open Ports Parser ... this is the important-ports string --> {mesg.group(1)}")

    return mesg.group(1)

def extract_all(logger, parts):
    server = extract_host_substring(logger, parts)
    ports = extract_ports_array(logger, parts)

    #logger.debug("Ports are ... %s", ports)

    return server, ports


class PortScanResponseParser(object):
    def __init__(self, logger, nmap_response):
        self.nmap_response = nmap_response
        self.logger = logger

    def split(self):
        return self.nmap_response.split(SEPARATOR)

    def run(self):
        parts = self.split()
        self.logger.debug(f"Parts : {parts}")

        return extract_all(self.logger, parts)
