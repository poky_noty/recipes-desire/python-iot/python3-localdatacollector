#from tcpclientserver import SEPARATOR, RESPONSE_TERMINATION_CHARS
from tcpclientserver import SEPARATOR, RESPONSE_TERMINATION_CHARS

from .host_discovery_book import HostDiscoveryBook

#from . import *

#from datacollector.main import logger


class HostDiscoveryToolParser(object):
    hosts_up = None
    host_name_ip = None

    def __init__(self, logger, nmap_str):
        self.logger = logger
        self.nmap_str = nmap_str

        logger.debug("*** Inside HostDiscoveryResponse Parser ***\n"
            "**** Ready to parse the lengthy-string of Up&Running ****\n"
            f"* {self.nmap_str} *\n"
            f"********** SEPARATOR is : {SEPARATOR} ***********")

    def from_string_to_list(self):
        # From String -> List
        self.hosts_up = self.nmap_str.split(SEPARATOR)
        self.logger.debug(f"Number of Lines in ServersUp NMapResponse is --> {len(self.hosts_up)}")

        return self.hosts_up

    #@pysnooper.snoop()
    def remove_first_characters(self, starting_str):
        length = len(starting_str)

        self.host_name_ip = list()
        for host in self.hosts_up:
            if host.startswith(starting_str):
                # Nmap Response for Host HostDiscovery
                host = host.strip()
                part_name_ip = host[length:]

                self.logger.debug("Name + IP is ... %s", part_name_ip)
                self.host_name_ip.append(part_name_ip)

    def split_remaining_name_ip(self):
        book = HostDiscoveryBook(self.logger)
        for name_ip in self.host_name_ip:
            try: 
                name, host = name_ip.split()
            except Exception as ex:
                self.logger.error(f"Invalid Server Name_IP that Cannot Split with Space Separator --> {name_ip}")
                continue

            if host.endswith(RESPONSE_TERMINATION_CHARS):
                # the end of the message
                l_ength = len(RESPONSE_TERMINATION_CHARS)
                host = host[0:len(host) - l_ength]

            # Remove parentheses from nmap returned IP
            host = host.replace("(", "")
            host = host.replace(")", "")

            self.logger.debug("Name is ... %s && IP is %s !", name, host)

            book.append_host_up(name, host)

        self.logger.debug("Magnificent Book of Servers Created !")

        return book
