class HostDiscoveryBook(object):
    hosts_up = None

    def __init__(self, logger=None):
        self.hosts_up = list()
        self.logger = logger 

        self.logger.debug("!!! Ready to Create a Servers-Up Book !!!")

    def append_host_up(self, name, host):
        self.logger.debug(f"New Server for Book: {name} , {host}")
        self.hosts_up.append({"name": name, "ip": host})
        self.logger.debug(f"After Appending New Server in Book of Ups --> {len(self.hosts_up)}")

    def num_of_entries(self):
        return len(self.hosts_up)

    def __str__(self):
        my_message = ""
        for host in self.hosts_up:
            my_message += host["name"] + " ... " + host["ip"] + " , "

        return my_message

    def __iter__(self):
        return self.hosts_up.__iter__()

    def __next__(self):
        #return self.hosts_up.__next__()
        pass

    def ips_to_array(self):
        # Use LIST-COMPREHENSION
        ips_arr = [item["ip"] for item in self.hosts_up]

        return ips_arr

    def to_array(self):
        return self.hosts_up
