from datetime import datetime

from datacollector.external_configuration import FileConfigurator
from datacollector.local_logger import LocalLogger

from datacollector.control_unit import main

#logger = None


def run_external_config_demo():
    '''
    A helper function to call External-Configuration for testing purposes.

    :param string full_pathname: the .CONF file to read {folder + name}
    '''
    #global logger

    file_configurator = FileConfigurator()
    file_configurator.read_config_data()
    file_configurator.parse_sections()

    # external configuration from DEFAULT section in .CONF file
    default_config_data = file_configurator.default_config_data

    # Setup local-logger and start writing to /var/log/datacolllector.log
    local_logger = LocalLogger(default_config_data)
    local_logger.setup()

    logger = local_logger.logger
    logger.debug("Welcome to Local-Data-Collector .. Ready to Meet the Backend Services %s", datetime.now())
    # Logger is enabled

    # One(1) ==> NMAP TCP-Agent
    nmap_config_data = file_configurator.nmap_config_data
    logger.debug("External Configuration for NMAP Connection is ... %s: %d", nmap_config_data.ip,
                nmap_config_data.port)

    # Two(2) ==> Redis message-queue
    redismq_config_data = file_configurator.redismq_config_data
    logger.debug("External Configuration for RedisMQ Connection is ... %s: %d", redismq_config_data.ip,
                redismq_config_data.port)

    # Three(3) ==> MQTT-Broker Local
    mqttbroker_local = file_configurator.mqttbroker_local
    logger.debug("External Configuration for MQTTBroker Connection is ... %s: %d", mqttbroker_local.ip,
                mqttbroker_local.port)

    # MQTT-Broker Central
    #mqttbroker_central = file_configurator.mqttbroker_central
    #logger.info("MQTTBroker-Central Connection ... %s: %d",
    #            mqttbroker_central.ip, mqttbroker_central.port)

    # Scheduler Frequencies
    # -----------------------
    scheduler_frequencies = default_config_data.scheduler_frequencies
    logger.debug("External Configuration for Scheduler Frequencies ... for Hosts-Up & Ports-Open ... %s",
                scheduler_frequencies)

    # Is-Daemon or Standalone-Application
    is_daemon = default_config_data.is_daemon
    if is_daemon:
        logger.debug("Run Data-Collector as Daemon !")
        #from datacollector.daemon_tools import daemon_call
        #daemon_call(redismq_config_data, nmap_config_data)
    else:
        logger.debug("... Ready to Run Data-Collector as a Stand-Alone Application ...")
        main(logger, redismq_config_data, nmap_config_data, mqttbroker_local,
             scheduler_frequencies)
