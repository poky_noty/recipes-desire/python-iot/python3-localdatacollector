from mastermind.validate_collected_data import ValidateCollectedData 
from mastermind.comparison_rules_names import ComparisonRulesNames

from .redis_communication_manager import get_all_hosts_up, get_open_ports_of_server


# Import ... Logger
#from . import *

#from datacollector.main import logger
def is_server_down(logger, hosts_up_collected_now, client):
    '''
    Validate and Compare the list of ... Currently Active Servers VS Stored
    '''

    # Call an AI entitie .... class ValidateCollectedData .. from MasterMind subproject
    hosts_up_stored = get_all_hosts_up(client)
    if not hosts_up_stored:
        # Empty results set from redis
        logger.error("Aborting Validation of Hosts-Up ... Empty Stored")
        return

    if not hosts_up_collected_now:
        # Problem with Collected data ... Empty result set
        logger.error("Aborting Validation of Hosts-Up ... Empty Collected")
        return

    hosts_up_ips_arr = hosts_up_collected_now.ips_to_array()

    ValidateCollectedData(
        logger, hosts_up_ips_arr, hosts_up_stored,
        ComparisonRulesNames.EQUAL).compare_collected_stored()


def is_port_closed(logger, port_scan_book, client):
    '''
    NMap collected new data for open-ports during the latest run cycle.
    Must be validated and compared to stored data ... need decisions-made for ports-closed.
    '''

    logger.debug("Datapipe for Master-Mind: Validate Latest Open Ports Found")

    # Get a list of Stored HostsUp
    hosts_up = get_all_hosts_up(client)

    #logger.debug( "Get-All-Hosts-Up-From-Datastore() --> %s", hostsUp )

    # Get OpenPorts for every HostUp
    for host in hosts_up:
        #logger.debug("Host-Up from Datastore --> %s", host )
        open_ports = get_open_ports_of_server(logger, client, host)
        #logger.debug("Open-Ports of Server from Datastore --> %s", openPorts)

    #get_stored_hosts_up()
    ValidateCollectedData(logger, port_scan_book, open_ports, ComparisonRulesNames.EQUAL).compare_collected_stored()
