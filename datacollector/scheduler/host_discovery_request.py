


from tcpclientserver.client import send_request

from .master_mind_pipe import is_server_down

#from ..vlab.monitoring.host_discovery_book import HostDiscoveryBook

#from . import *

##from ..main import logger
from ..vlab.monitoring.host_discovery_tool_parser import HostDiscoveryToolParser

HOST_DISCOVERY_REQ = "nmap:host:discovery!"

FIRST_CHARACTERS = "Nmap scan report for"

#NMAP_AGENT_IP = "0.0.0.0"
#NMAP_AGENT_PORT = 5000
def validate_new_host_data(logger, collected_data, client):
    '''
    Compare the just-collected data with already-stored and see if differences exist
    associated with server-down cases.
    '''
    is_server_down(logger, collected_data, client)
    #pass


#@pysnooper.snoop()
def perform_host_discovery(logger, nmap_wrapper_ip, nmap_wrapper_port, client):
    #now = datetime.datetime.now()
    #logger.debug("Ready to Send Host Discovery Request to NMAP-Agent ... %s!", now)

    # Send Request to TCP-Server & receive list of hosts_up
    # The response is a lengthy STRING
    logger.debug("*** Ready to Start the Running Servers Hunting !")
    hostsup = send_request(logger, nmap_wrapper_ip, nmap_wrapper_port,
                           HOST_DISCOVERY_REQ)
    logger.debug("*** Many Running Servers Found in Subnet Squid !")
    

    parser = HostDiscoveryToolParser(logger, hostsup)
    hosts_up = parser.from_string_to_list()
    parser.remove_first_characters(FIRST_CHARACTERS)
    hosts_book = parser.split_remaining_name_ip()

    logger.debug("Book of Hosts-Up ... Number of Servers Found = %d", hosts_book.num_of_entries())
    #logger.debug( "Book of Hosts-Up ... %s", hosts_book )

    for host in hosts_up:
        logger.debug("Hosts-Up Response ... Server Found = %s", host)

    validate_new_host_data(logger, hosts_book, client)

    # Bad Practice: Used Only at Application Start-Up ... No data saved to local store
    return hosts_book
