
from tcpclientserver.client import send_request
from datacollector.vlab.monitoring.port_scan_response_parser import PortScanResponseParser

from datacollector.vlab.monitoring.port_scan_book import PortScanBook
from datacollector.vlab.monitoring.host_discovery_book import HostDiscoveryBook

from .redis_communication_manager import get_all_hosts_up
from .master_mind_pipe import is_port_closed

MSG_PORT_SCAN = "nmap:port:scan"


def validate_port_scan_data(logger, book, client):
    try:
        is_port_closed(logger, book, client)
    except Exception as x_except:
        logger.debug("Problem with Data Validation --> %s", x_except)


def perform_port_scanning_servers(_logger, _nmap_wrapper_ip, _nmap_wrapper_port,
                                  client):
    #global logger
    logger = _logger
    #global nmap_wrapper_ip
    nmap_wrapper_ip = _nmap_wrapper_ip
    #global nmap_wrapper_port
    nmap_wrapper_port = _nmap_wrapper_port

    hosts_book = get_all_hosts_up(client)
    if not hosts_book:
        # No list of HostsUp from Redis
        logger.error("*** "+
                     "Cannot Proceed with Ports-Scan ... Failed to Get HostsUp from Redis "+
                     "***")
        return None
        
    port_scan_book = perform_port_scanning(logger, hosts_book,
                                           nmap_wrapper_ip, nmap_wrapper_port)

    validate_port_scan_data(logger, port_scan_book, client)

    # Bad Practice : Only used when NO data found in Redis ... First scan local nertwork\
    # with nmap and after save
    return port_scan_book

def perform_port_scanning(logger, servers_book, nmap_wrapper_ip, nmap_wrapper_port):
    '''
    Having a list o Hosts_Up {} and needing to examine each server for open-ports.
    '''

    # Send Request to NMap-Agent using a TCP-Socket
    #global port_scan_book
    port_scan_book = PortScanBook()

    for server in servers_book:
        open_ports_msg = ""
        if isinstance(servers_book, HostDiscoveryBook):
            open_ports_msg = create_port_scanning_request(
                logger, nmap_wrapper_ip, nmap_wrapper_port, server["ip"],
                MSG_PORT_SCAN)
        else:
            open_ports_msg = create_port_scanning_request(
                logger, nmap_wrapper_ip, nmap_wrapper_port, server, MSG_PORT_SCAN)

        if not open_ports_msg:
            # No response-message for open-ports from NMAP
            #raise Exception("Empty Response from NMap for Open-Ports")
            logger.warn("Ports Scanning for Server '%s' returned an Empty Result-Set !",
                        server)
            return None

        # Parse the NMap response ...
        parser = PortScanResponseParser(logger, open_ports_msg)
        # Which Server & Ports
        server, ports = parser.run()

        port_scan_book.add_server_with_open_ports(server=server, ports=ports)

        logger.debug("***** Open Ports Book: +=%s [%s] ... #%s",
                     server, len(ports), ports)

    return port_scan_book


def create_port_scanning_request(logger, nmap_agent, nmap_port, host_up,
                                 port_scanning_req):
    '''
    Send a Port-Scanning Request for a Server in Hosts-Up list.    
    '''
    complex_msg = port_scanning_req + ":" + host_up + ":!"
    open_ports_msg = send_request(logger, nmap_agent, nmap_port, complex_msg)

    return open_ports_msg
