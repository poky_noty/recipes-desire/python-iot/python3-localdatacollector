from messageqclient.contact_message_queue import check_datastore_availability, check_key_existence,\
    delete_data_from_db, get_all_values_from_set
from messageqclient.contact_message_queue import get_open_ports_for_server, append_end,\
    append_start, save_open_ports_for_server, my_keys_list
from messageqclient.constants import KeyNames



def check_datastore(logger, host, port, datab):
    #global logger
    #logger = loggerIn

    client = check_datastore_availability(logger, host, port, datab)
    return client

    #return True

def check_hosts_up_existence(client):
    key = KeyNames().hostsup
    return True if check_key_existence(key, client) == 1 else False


def delete_net_data(logger, client):
    #global logger
    response = delete_data_from_db(client)
    logger.debug("Response of DB-Flush Request: %s", response)


def get_all_hosts_up(client):
    key = KeyNames().hostsup
    values = get_all_values_from_set(client, key)

    # Redis response contains b'my-string'
    #return values
    return [v.decode() for v in values]


def get_open_ports_of_server(logger, client, server_key="dummyserver"):
    #open_ports = get_all_values_from_set( server_key )
    open_ports = get_open_ports_for_server(logger, server_key, client)

    return open_ports


def set_open_ports_of_server(server_key, ports_list, client):
    append_end(server_key, client, ports_list, "REDIS")


def store_hosts_up(hosts_book, client):
    key = KeyNames().hostsup
    values = hosts_book.ips_to_array()
    #logger.debug("Ready to Store Hosts: %s", values)

    #key = "pipis"
    #values = ["1","2"]
    append_start(key, values, client)


def store_open_ports(logger, ports_book, client):
    #logger.debug( "Ports-Book First Page(s) ... %s", ports_book[0] )

    for record in ports_book:
        # Server && Open-Ports
        examined_server = record["server"].strip()

        if examined_server.endswith("@endofresponse@@"):
            # Last server in NMAP response for active ones
            examined_server = examined_server.replace("@endofresponse@@", "")

        open_ports_l = record["ports"].strip()

        #logger.debug("--> %s ... %s", examined_server, open_ports_l)
        save_open_ports_for_server(logger, examined_server, open_ports_l, client)

    return


def get_list_of_keys_in_db(logger, client):
    # Get a list of all my keys
    all_keys = my_keys_list(client)

    logger.debug("List of Stored Keys in Redis --> \n%s", all_keys)
