import time
#import os

import schedule

#logger = None
#nmap_wrapper_ip = None
#nmap_wrapper_port = None

from .host_discovery_request import perform_host_discovery
from .port_scanning_request import perform_port_scanning_servers


def simple_job_host(logger, wrapper_ip, wrapper_port, client):
    #global nmap_wrapper_ip, nmap_wrapper_port
    logger.debug("Ready to Send Host Scanning Request at ... %s:%d",
                 wrapper_ip, wrapper_port)
    perform_host_discovery(logger, wrapper_ip, wrapper_port, client)


def simple_job_port(logger, wrapper_ip, wrapper_port, client):
    '''
    Search for Open-Ports using a frequent time scheme
    '''
    #global logger
    #global nmap_wrapper_ip
    #global nmap_wrapper_port
    logger.debug("Ready to Send Port Scanning Request at ... %s:%d",
                 wrapper_ip, wrapper_port)
    
    perform_port_scanning_servers(logger, wrapper_ip, wrapper_port, client)


def simple_test(logger, freq_host, freq_port, wrapper, client):
    # PRINT() ... It is not working in case of subprocess-context
    #print("Simple Scheduler Land")
    if freq_host["units"] == "MINUTES":
        logger.debug(
            "Ready to Schedule Frequent Host-Discovery .... Every %d minute(s)",
            freq_host["value"])
        schedule.every(freq_host["value"]).minutes.do(simple_job_host, logger=logger,
                                                   wrapper_ip=wrapper["ip"],
                                                   wrapper_port=wrapper["port"], 
                                                   client=client)
    elif freq_host["units"] == 'HOURS':
        logger.debug(
            "Ready to Schedule Infrequent Host-Discovery .... Every %d hour(s)",
            freq_host.value)
        schedule.every(freq_host["value"]).hours.do(simple_job_host, logger=logger,
                                                 wrapper_ip=wrapper["ip"],
                                                 wrapper_port=wrapper["port"],
                                                 client=client)
    else:
        raise NotImplementedError(
            "Try Another Unit for Frequency of Host Discovery")

    if freq_port["units"] == "MINUTES":
        logger.debug(
            "Ready to Schedule Frequent Port-Scanning .... Every %d minute(s)",
            freq_port["value"])
        schedule.every(freq_port["value"]).minutes.do(simple_job_port, logger=logger,
                                                   wrapper_ip=wrapper["ip"],
                                                   wrapper_port=wrapper["port"],
                                                   client=client)
    elif freq_port["units"] == 'HOURS':
        logger.debug(
            "Ready to Schedule Infrequent Port-Discovery .... Every %d hour(s)",
            freq_port["value"])
        schedule.every(freq_port["value"]).hours.do(simple_job_port, logger=logger,
                                                 wrapper_ip=wrapper["ip"],
                                                 wrapper_port=wrapper["port"],
                                                 client=client)
    else:
        raise NotImplementedError(
            "Try Another Unit for Frequency of Port Scanning")

    while True:
        schedule.run_pending()
        time.sleep(1)


def scheduler_main(_logger, scheduler_config_data, _nmap_wrapper, client):
    #global logger
    logger = _logger

    logger.debug("* Crossing Scheduler's Gate *\n"+
                 "NMAP-Wrapper is ... %s", _nmap_wrapper.ip)

    #global nmap_wrapper_ip
    wrapper = {
        "ip": _nmap_wrapper.ip,
        "port":_nmap_wrapper.port
        }

    #HOST_DISCOVERY_FREQ = os.environ[ 'HOST-DISCOVERY-FREQ']
    host_discovery_freq = scheduler_config_data['host_scan_freq']
    freq_host = host_discovery_freq.split("-")
    freqhost = {
        "value": int(freq_host[0]),
        "units": str(freq_host[1])
        }

    #PORT_SCANNING_FREQ = os.environ[ 'PORT-SCANNING-FREQ']
    port_scanning_freq = scheduler_config_data['ports_scan_freq']
    freq_port = port_scanning_freq.split("-")
    freqport = {
        "value": int(freq_port[0]),
        "units": str(freq_port[1])
        }

    logger.debug("Host-disc-freq = %s - %s ... Port-scan-freq = %s - %s",
                 freqhost["value"], freqhost["units"], 
                 freqport["value"], freqport["units"])

    simple_test(logger, freqhost, freqport, wrapper, client)
