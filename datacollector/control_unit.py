import datetime

#from . import *

from .scheduler.redis_communication_manager import check_hosts_up_existence,\
    store_open_ports, get_all_hosts_up, store_hosts_up
from .scheduler.redis_communication_manager import delete_net_data,\
    get_list_of_keys_in_db, check_datastore

from .scheduler.host_discovery_request import perform_host_discovery
from .scheduler.port_scanning_request import perform_port_scanning
from .scheduler.simple_scheduler import scheduler_main

#from datacollector.main import logger

#from configparser import ConfigParser
#config = ConfigParser()
#config.read("/etc/datacollector/datacollector.conf")


from .scheduler.ping_pong_test import check_nmap_agent_listening
from .scheduler.mqttbroker_expert import check_broker
from time import sleep

SLEEP_SECS = 6

#####
## In case of Standalone-Application ... Entrypoint
###
def main(logger, redismq_config_data, nmapagent_config_data, mqttbroker_config_data,
         scheduler_frequencies):
    start_time = datetime.datetime.now()
    logger.debug("Starting Local-Data-Collector at ... %s", start_time)

    # Step 1: Check Redis Availability (In case of No Redis Available an Exception is Thrown)
    # ------
    num_attempts = 0
    while True: 
        num_attempts += 1
        logger.debug("Ready to Test Connection with Redis Message-Queue ... Attempt = %d",
                     num_attempts)
        
        client = check_datastore(
            logger, redismq_config_data.ip, redismq_config_data.port, datab=0)
        
        if not client:
            logger.error(
                "Cannot Communicate with Redis-MQ at %s:%d ... Trying Again !",
                redismq_config_data.ip, redismq_config_data.port)
            
            sleep(SLEEP_SECS)
            #logger.error("**** ABORTING LOCAL-DATA-COLLECTOR ****")
            #return
        else:
            # Success
            logger.debug("*** Connection with Redis-MQ is VERIFIED (OK) ***")
            
            break

    # Step 2: Check NMAP-Agent Connectivity (In case of NO NMAP-Agent is
    # listening an Exception is Thrown)
    num_attempts = 0
    while True:
        num_attempts += 1
        
        logger.debug("* Ready to Test Connection with NMAP-Server ... Attempt = %d",
                     num_attempts)
        
        if not check_nmap_agent_listening(logger, nmapagent_config_data.ip,
                                          nmapagent_config_data.port):
            logger.error("Cannot Communicate with NMAP-Server at %s: %d ... "+
                         "Trying Again", 
                         nmapagent_config_data.ip, nmapagent_config_data.port)

            #logger.error("**** ABORTING LOCAL-DATA-COLLECTOR ****")
            #return
            
            sleep(SLEEP_SECS)
        else:
            # NMAP-Server Connection is OK 
            logger.debug("Connection with NMAP-Server is Established ... "+
                         "Number of Attempt = %d", num_attempts)
            break

    # Step 3: Check Local MQTT-Broker Availability ... What should we do in case of Broker-Missing
    # ------
    if not check_broker(logger, mqttbroker_config_data.ip,
                        mqttbroker_config_data.port, keepalive=60, topic="#"):
        logger.error(
            "Cannot Communicate with Local Mosquitto MQTT-Broker at %s: %d ... Aborting !",
            mqttbroker_config_data.ip, mqttbroker_config_data.port)
        return

    #return

    # ************************************
    # Should I clean the datastore first ?
    # ************************************
    start_with_fresh_datastore = redismq_config_data.start_with_fresh
    if start_with_fresh_datastore:
        # Delete old content
        delete_net_data(logger, client)

    #set_open_ports_of_server()
    #logger.debug( "----> %s", get_open_ports_of_server() )

    ### EndOF Step 1 !!!

    # Step 2: Let the application check whether ...
    # ------
    #    --> the "HostsUp" data is stored at Redis
    #    --> in case of no HostsUp data ... first execution cycle & data must gathered first
    if check_hosts_up_existence(client):
        # ###
        # Found stored data in Redis ... HostsUp !
        logger.debug(
            "Found HostsUp stored data in Redis ... From Previous Network Scan")

        hosts_book = get_all_hosts_up(client)
        logger.debug("Found in Datastore these Hosts-Up ... %s", hosts_book)
    else:
        # ###
        # No HostsUp found in Redis .. Need to send Immediately a New Nmap request
        logger.debug(
            "No HostsUp data Found in Datastore ... Sending Immediately Request to NMAP " +
            "(Not Waiting the Scheduler)"
        )
        
        try:
            hosts_book = perform_host_discovery(logger, nmapagent_config_data.ip,
                                            nmapagent_config_data.port, client)
        except Exception as ex:
            logger.error(f"PerformHostDiscovery Raised an Exception ... {str(ex)}")
            exit(-1)
        
        logger.debug("After NMAP-Request for Host Discovery ... Number of Hosts Found %d",
                     #len(hosts_book))
                     hosts_book.num_of_entries())

        # "CLIENT" is connected to redis-mq 
        store_hosts_up(hosts_book, client)
    ### EndOF Step 2: Hosts-Up {} are available

    # Step 3: AFter having a long list of hosts_up, it is time to examine
    # open-ports in each of the servers
    ports_book = perform_port_scanning(logger, hosts_book, nmapagent_config_data.ip,
                                       nmapagent_config_data.port)

    store_open_ports(logger, ports_book, client)
    get_list_of_keys_in_db(logger, client)

    # Start scheduler
    scheduler_main(logger, scheduler_frequencies, nmapagent_config_data, client)
