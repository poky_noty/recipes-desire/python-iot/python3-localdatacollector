on target:

pydeps ./usr/lib/python3.7/site-packages/datacollector --show-dot --noshow --pylib

on host:

copy over the result to host as datacollector.dot

dot -Tsvg datacollector.dot -o datacollector.svg
